//======L-SHAPE=========


const renderScreenWidthOpacityShape = $("#opacityRenderScreen").width();
const renderScreenHeightOpacityShape = $("#opacityRenderScreen").width();
const sceneOpacityShape = new THREE.Scene();
const cameraOpacityShape = new THREE.PerspectiveCamera( 75, renderScreenWidthOpacityShape / renderScreenHeightOpacityShape, 0.1, 1000 );
cameraOpacityShape.position.z = 5;

const rendererOpacityShape = new THREE.WebGLRenderer();
rendererOpacityShape.setSize( renderScreenWidthOpacityShape, renderScreenHeightOpacityShape );//setSize(width, height)
$("#opacityRenderScreen").append( rendererOpacityShape.domElement );

//Make scrollable
const controlsOpacityShape = new THREE.OrbitControls( cameraOpacityShape, rendererOpacityShape.domElement);

const sphere2 = new THREE.SphereGeometry( 0.5, 16, 8 );
// const light_OpacityShape = new THREE.PointLight( 0x000000, 1, 0, 2 );
// light_OpacityShape.position.set( 10, 10, 10 );
// sceneOpacityShape.add( light_OpacityShape );
light_OpacityShape = new THREE.PointLight( 0xffffff, 2, 50 );
// light_OpacityShape.add( new THREE.Mesh( sphere2, new THREE.MeshBasicMaterial( { color: 0xff0040 } ) ) );
sceneOpacityShape.add( light_OpacityShape );
light_OpacityShape.position.set(1, 2, 2);
console.log(light_OpacityShape);

// const secondarylight_OpacityShape = new THREE.PointLight( 0xffffff, .5, 0, 2 );
// secondarylight_OpacityShape.position.set( -50, 50, 50 );
// sceneOpacityShape.add( secondarylight_OpacityShape );

//this light makes the bottom visible but still a bit gray
const ambientLight_OpacityShape = new THREE.AmbientLight( 0x404040 ); //soft white light
sceneOpacityShape.add( ambientLight_OpacityShape );

const colors_OpacityShape = new Float32Array([
    0, 256, 0,
    0, 256, 0,
    0, 256, 0,
    256, 0, 0,
    0, 256, 0,
    256, 0, 0,
    0, 256, 0,
    0, 256, 0,
]);


const lengthA_OpacityShape = .5;
const lengthB_OpacityShape =  1.0;

const loader2 = new THREE.GLTFLoader();
loader2.load('../../objects/l-shape-lowPoly.glb', function(gltf){
        OpacityShape = gltf;


        var geometry_OpacityShape = OpacityShape.scene.children[0].geometry;
        // geometry_OpacityShape.setAttribute( 'color', new THREE.BufferAttribute( colors_OpacityShape, 3, true));
        // Even though color is specified in the geometry, a material is still required
        var material_OpacityShape = new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors, side: THREE.DoubleSide});

        var opaque_material = new THREE.MeshStandardMaterial({ color: "#FFF", transparent: true, side: THREE.DoubleSide, alphaTest: 0.5 });

        var alphaMap = new THREE.TextureLoader().load('../../images/squareAlpha.png');
        opaque_material.alphaMap = alphaMap;
        opaque_material.alphaMap.magFilter = THREE.NearestFilter;
        opaque_material.alphaMap.wrapT = THREE.RepeatWrapping;
        // opaque_material.alphaMap.repeat.y = 1;
        // opaque_material.alphaMap.repeat.x = 1;
        opaque_material.format = THREE.RGBAFormat;
        opaque_material.transparent = true;

        var texture = new THREE.TextureLoader().load('../../images/bars01.png');
        var testMaterial = new THREE.MeshBasicMaterial({ map: texture });

        var OpacityShapeMesh = new THREE.Mesh(geometry_OpacityShape, opaque_material);
        console.log(OpacityShapeMesh);
        OpacityShapeMesh.material.alphaMap.offset.y = -0.5;
        OpacityShapeMesh.material.alphaMap.offset.x = -0.5;

        // sceneOpacityShape.add(gltf.scene);
        sceneOpacityShape.add(OpacityShapeMesh);

        $("input").on('input', function() {
            changeOpacityShape(geometry_OpacityShape);
        });
        // changeOpacityShape(OpacityShape.scene.children[0].geometry);

    }, undefined, function( error ) {
        console.error( error );
    }
);


fillInInputs();
function fillInInputs(){
    $("#lengthA").val(lengthA_OpacityShape);
    $("#lengthB").val(lengthB_OpacityShape);
}

function changeOpacityShape(geometry_OpacityShape){
    //De edge H-G moet verschuiven langs de Z-as om lengte A langer te maken
    //De edge C-F moet verschuiven langs de Y-as om lengte B langer te maken
    var By = 1;
    var Az = 1;
    var newvertices_OpacityShape = new Float32Array( [
        1,  1, Az,    //A vast
        1,  By, 1,    //B vast
        1, By - $("#lengthB").val(), 1,    //C
        -1,  1, 1,    //D
        -1,  1, 1,    //E
        -1, By - $("#lengthB").val(), 1,    //F
        1,  1, Az - $("#lengthA").val(), //G
        -1,  1, Az - $("#lengthA").val()  //H
    ] );

    geometry_OpacityShape.attributes.position.array = newvertices_OpacityShape;
    geometry_OpacityShape.attributes.position.needsUpdate = true;
    rendererOpacityShape.render( sceneOpacityShape, cameraOpacityShape );
}



animate();
function animate() {
    requestAnimationFrame( animate );
    // required if controlsOpacityShape.enableDamping or controlsOpacityShape.autoRotate are set to true
    // controlsOpacityShape.update();
    rendererOpacityShape.render( sceneOpacityShape, cameraOpacityShape );
}

//======SQUARE=========
const renderScreenWidth = $("#squareRenderScreen").width();
const renderScreenHeight = $("#squareRenderScreen").width();
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, renderScreenWidth / renderScreenHeight, 0.1, 1000 );
camera.position.z = 5;

const renderer = new THREE.WebGLRenderer();
renderer.setSize( renderScreenWidth, renderScreenHeight );//setSize(width, height)
$("#squareRenderScreen").append( renderer.domElement );

//Make scrollable
const controls = new THREE.OrbitControls( camera, renderer.domElement);
// controls.update() must be called after any manual changes to the camera's transform
// camera.position.set( 0, 0, 5 );
// controls.update();

const light = new THREE.PointLight( 0xff0000, 10, 100 );
light.position.set( 50, 50, 50 );
scene.add( light );

const topLeftX = -1.0;
const topLeftY =  1.0;
const topRightX = 1.0;
const topRightY = 1.0;
const bottomLeftX = -1.0;
const bottomLeftY = -1.0;
const bottomRightX = 1.0;
const bottomRightY = -1.0;
const geometry = new THREE.BufferGeometry();
// create a simple square shape. We duplicate the top left and bottom right
// vertices because each vertex needs to appear once per triangle.
//   (-1, 1)----(1, 1)       (3)----(2)
//    ---------------         ---------
//    ---------------         ---------
//    (-1,-1)---(1,-1)        (0)---(1)
const vertices = new Float32Array( [
    bottomLeftX, bottomLeftY,  1.0, //0
    bottomRightX, bottomRightY,  1.0,  //1
    topRightX,  topRightY,  1.0,  //2

    // 1.0,  1.0,  1.0,  //2  -> removed cause we used setIndex later on to reference this one
    topLeftX,  topLeftY,  1.0, //3
    // -1.0, -1.0,  1.0 //0   -> removed cause we used setIndex later on to reference this one
] );
const colors = new Float32Array( [
    256, 0, 0,
    0, 256, 0,
    0, 0, 256,
    125, 125, 0
] );

// itemSize = 3 because there are 3 values (components) per vertex
const positionAttribute = new THREE.BufferAttribute( vertices, 3 );
positionAttribute.setUsage(THREE.DynamicDrawUsage);
geometry.setAttribute( 'position',  positionAttribute);
const material = new THREE.MeshBasicMaterial( { color: 0xffffff, vertexColors: true } );
geometry.setAttribute( 'color', new THREE.BufferAttribute( colors, 3 ));
console.log(geometry);
const mesh = new THREE.Mesh( geometry, material );

// simplify the vertices by using indexes as reference instead of double naming
geometry.setIndex([
    0, 1, 2,   2, 3, 0
]);

scene.add( mesh );
renderer.render( scene, camera );

fillInInputs();
function fillInInputs(){
    $("#corner-tl-x").val(topLeftX);
    $("#corner-tl-y").val(topLeftY);
    $("#corner-tr-x").val(topRightX);
    $("#corner-tr-y").val(topRightY);
    $("#corner-bl-x").val(bottomLeftX);
    $("#corner-bl-y").val(bottomLeftY);
    $("#corner-br-x").val(bottomRightX);
    $("#corner-br-y").val(bottomRightY);
}

function changeSquare(){
    var newVertices = new Float32Array( [
        $("#corner-bl-x").val(), $("#corner-bl-y").val(),  1.0, //0
        $("#corner-br-x").val(), $("#corner-br-y").val(),  1.0,  //1
        $("#corner-tr-x").val(),  $("#corner-tr-y").val(),  1.0,  //2
        $("#corner-tl-x").val(),  $("#corner-tl-y").val(),  1.0, //3
    ] );
    geometry.attributes.position.array = newVertices;
    geometry.attributes.position.needsUpdate = true;
    renderer.render( scene, camera );
}

$("input").on('input', function() {
    changeSquare();
});

animate();
function animate() {
    requestAnimationFrame( animate );
    // required if controls.enableDamping or controls.autoRotate are set to true
    // controls.update();
    renderer.render( scene, camera );
}

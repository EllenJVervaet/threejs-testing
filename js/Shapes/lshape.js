//======L-SHAPE=========


const renderScreenWidthLshape = $("#lshapeRenderScreen").width();
const renderScreenHeightLshape = $("#lshapeRenderScreen").width();
const sceneLshape = new THREE.Scene();
const cameraLshape = new THREE.PerspectiveCamera( 75, renderScreenWidthLshape / renderScreenHeightLshape, 0.1, 1000 );
cameraLshape.position.z = 5;

const rendererLshape = new THREE.WebGLRenderer();
rendererLshape.setSize( renderScreenWidthLshape, renderScreenHeightLshape );//setSize(width, height)
$("#lshapeRenderScreen").append( rendererLshape.domElement );

//Make scrollable
const controlsLshape = new THREE.OrbitControls( cameraLshape, rendererLshape.domElement);

const sphere = new THREE.SphereGeometry( 0.5, 16, 8 );
// const light_Lshape = new THREE.PointLight( 0x000000, 1, 0, 2 );
// light_Lshape.position.set( 10, 10, 10 );
// sceneLshape.add( light_Lshape );
light_Lshape = new THREE.PointLight( 0xffffff, 2, 50 );
// light_Lshape.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0xff0040 } ) ) );
sceneLshape.add( light_Lshape );
light_Lshape.position.set(1, 2, 2);

// const secondarylight_Lshape = new THREE.PointLight( 0xffffff, .5, 0, 2 );
// secondarylight_Lshape.position.set( -50, 50, 50 );
// sceneLshape.add( secondarylight_Lshape );

//this light makes the bottom visible but still a bit gray
const ambientLight_Lshape = new THREE.AmbientLight( 0x404040 ); //soft white light
sceneLshape.add( ambientLight_Lshape );

const colors_Lshape = new Float32Array([
    0, 256, 0,
    0, 256, 0,
    0, 256, 0,
    256, 0, 0,
    0, 256, 0,
    256, 0, 0,
    0, 256, 0,
    0, 256, 0,
]);


const lengthA_Lshape = .5;
const lengthB_Lshape =  1.0;

const loader = new THREE.GLTFLoader();
loader.load('../../objects/l-shape-lowPoly.glb', function(gltf){
        Lshape = gltf;

        var geometry_Lshape = Lshape.scene.children[0].geometry;
        geometry_Lshape.setAttribute( 'color', new THREE.BufferAttribute( colors_Lshape, 3, true));
        // Even though color is specified in the geometry, a material is still required
        var material_Lshape = new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors, side: THREE.DoubleSide});
        //This works!
        // Lshape.scene.children[0].material.color.setHex(0x00ff00);

        var LshapeMesh = new THREE.Mesh(geometry_Lshape, material_Lshape)

        // sceneLshape.add(gltf.scene);
        sceneLshape.add(LshapeMesh);

        $("input").on('input', function() {
            changeLShape(geometry_Lshape);
        });
        // changeLShape(Lshape.scene.children[0].geometry);

    }, undefined, function( error ) {
        console.error( error );
    }
);


fillInInputs();
function fillInInputs(){
    $("#lengthA").val(lengthA_Lshape);
    $("#lengthB").val(lengthB_Lshape);
}

function changeLShape(geometry_Lshape){
    //De edge H-G moet verschuiven langs de Z-as om lengte A langer te maken
    //De edge C-F moet verschuiven langs de Y-as om lengte B langer te maken
    var By = 1;
    var Az = 1;
    var newvertices_Lshape = new Float32Array( [
         1,  1, Az,    //A vast
         1,  By, 1,    //B vast
         1, By - $("#lengthB").val(), 1,    //C
        -1,  1, 1,    //D
        -1,  1, 1,    //E
        -1, By - $("#lengthB").val(), 1,    //F
         1,  1, Az - $("#lengthA").val(), //G
        -1,  1, Az - $("#lengthA").val()  //H
    ] );

    geometry_Lshape.attributes.position.array = newvertices_Lshape;
    geometry_Lshape.attributes.position.needsUpdate = true;
    rendererLshape.render( sceneLshape, cameraLshape );
}



animate();
function animate() {
    requestAnimationFrame( animate );
    // required if controlsLshape.enableDamping or controlsLshape.autoRotate are set to true
    // controlsLshape.update();
    rendererLshape.render( sceneLshape, cameraLshape );
}

//=== Variables 2D
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.save();
var padding = 30;
var recordClicks = false;
var recordedClicks = [];

var splitPlaneTool = {
        isSplittingPlane: false,
        isChoosingPart: false,
        finishedSplitting: false,
        seperationLinePoints: {
            startPoint: { x: null, y: null, edge: null, edgeLabel: null },
            endPoint: { x: null, y: null, edge: null, edgeLabel: null }
        },
        splittedPart1:{
            chosenPart: false,
            path: null,
            points: []//{x: , y: , edge: , edgeLabel: }
        },
        splittedPart2:{
            chosenPart: false,
            path: null,
            points: []//{x: , y: , edge: , edgeLabel: }
        }
};

//other
var allowedMargin = 5;
var scale = 0;
var lengthFaces = [];
var heightPlate = 0;
var widthPlate = 0;

//Linker boven hoek van de volledige figuur (=bounding box)
var boundingBoxX = padding;
var boundingBoxY = padding;

drawFigure();
$("input").on('input', function() {
    drawFigure();
});
$("select").on('change', function() {
    drawFigure();
});
$("#recordClicks-btn").on('click', function() {
    recordClicks = !recordClicks;
    if(recordClicks){
        $("#recordClicks-btn").val("Stop met tekenen");
    } else {
        $("#recordClicks-btn").val("Tekenen");
    }
});
$("#recordSplitPlane-btn").on('click', function() {
    splitPlaneTool.isSplittingPlane = true;
    $('.js-helper-bubbletext').text("Selecteer 2 punten op de randen die de snij-lijn zullen vormen.")
    $('.js-show-bubble').show();
    $("#helpLabelSplitting").text("Teken de lijn waarmee je de plaat wil splitsten door op de randen 2 punten te tekenen.");
    // if(splitPlaneTool.isSplittingPlane){
    //     $("#recordSplitPlane-btn").val("Tekenen");
    // } else {
    //     $("#recordSplitPlane-btn").val("Tekenen");
    // }
});
$("#myCanvas").on("click", function(e){
    var localScale = scale == 0 ? 1 : scale;
    var x = (e.pageX - $("#myCanvas").position().left)/ localScale;
    var y = (e.pageY - $("#myCanvas").position().top) / localScale
   if(recordClicks && drawWithinFigure(x, y)){
       drawClicks(x, y);
   }
});

$("#myCanvas").on("mousemove", function(e){
    var bounds = e.target.getBoundingClientRect();
    var x = e.clientX - bounds.left;
    var y = e.clientY - bounds.top;

    if(splitPlaneTool.isSplittingPlane){
        ctx.clearRect(0, 0, 5000, 5000);
        drawFigure();

        //Draw the pointer on the edges
        ctx.beginPath();
        ctx.strokeStyle = "#e72222";
        var closestEdge = findClosestEdgePoint(x/scale, y/scale);
        drawEllipse(
            closestEdge.x - 2,
            closestEdge.y - 2 ,
            4,
            4
        );
        ctx.stroke();

        //Draw the seperation line from the first clicked point to your mouse
        if(splitPlaneTool.seperationLinePoints.startPoint.x != null && splitPlaneTool.seperationLinePoints.endPoint.x == null){ //startpoint but no endpoint
            ctx.beginPath();
            ctx.strokeStyle = "#ff0000";
            ctx.moveTo(splitPlaneTool.seperationLinePoints.startPoint.x, splitPlaneTool.seperationLinePoints.startPoint.y);
            ctx.lineTo(closestEdge.x, closestEdge.y);
            ctx.stroke();
        }
    }
    //Color the hovered plane
    else if(splitPlaneTool.isChoosingPart){
        //Inside part 1
        if(ctx.isPointInPath(splitPlaneTool.splittedPart1.path, x, y, "nonzero")){
            //Clear previously drawn
            //Todo: should we have a seperate clear function?
            drawFigure();
            //Hatch deleted square and make selected square white (order is important for z-index)
            strokePolygon(splitPlaneTool.splittedPart2.points, "white");
            fillPolygon(splitPlaneTool.splittedPart1.path, "white");
            strokePolygon(splitPlaneTool.splittedPart1.points, "#D60B52");
            hatchPolygon(splitPlaneTool.splittedPart2.points);
        }
        //Inside part 2
        // if(ctx.isPointInPath(splitPlaneTool.splittedPart2.path, x, y, "nonzero")){
        else{
            //Clear previously drawn
            //Todo: should we have a seperate clear function?
            drawFigure();
            //Hatch deleted square and make selected square white (order is important for z-index)
            strokePolygon(splitPlaneTool.splittedPart1.points, "white");
            fillPolygon(splitPlaneTool.splittedPart2.path, "white");
            strokePolygon(splitPlaneTool.splittedPart2.points, "#D60B52");
            hatchPolygon(splitPlaneTool.splittedPart1.points);
        }
    }

});

$("#myCanvas").on("click", function(e){
    var bounds = e.target.getBoundingClientRect();
    var x = e.clientX - bounds.left;
    var y = e.clientY - bounds.top;

    if(splitPlaneTool.isSplittingPlane){
        var closestEdgePoint = findClosestEdgePoint(parseFloat(x/scale).toFixed(0), parseFloat(y/scale).toFixed(0));
        //Second click cant be on the same edge as the first
        if(splitPlaneTool.seperationLinePoints.startPoint.x != null && splitPlaneTool.seperationLinePoints.endPoint.x == null &&
            splitPlaneTool.seperationLinePoints.startPoint.edge == closestEdgePoint.edge){
            alert('Your second point should be placed on another edge than the first');
        }else{
            //Save points if line is not completed
            if( splitPlaneTool.seperationLinePoints.startPoint.x == null){ // no startPoint yet
                splitPlaneTool.seperationLinePoints.startPoint = closestEdgePoint; }
            else if( splitPlaneTool.seperationLinePoints.endPoint.x == null){ //no endPoint yet
                splitPlaneTool.seperationLinePoints.endPoint = closestEdgePoint; }

            //Go to the next phase if the line is completed
            if(splitPlaneTool.seperationLinePoints.endPoint.x != null){ //both start and endpoint provided
                splitPlaneTool.isSplittingPlane = false;
                splitPlaneTool.isChoosingPart = true;
                $('.js-helper-bubbletext').text("Klik op het deel van de plaat dat je wilt behouden.")
                splittedParts = createTwoParts();
                splitPlaneTool.splittedPart1.points = splittedParts[0];
                splitPlaneTool.splittedPart1.path = createPath(splittedParts[0]);
                splitPlaneTool.splittedPart2.points = splittedParts[1];
                splitPlaneTool.splittedPart2.path = createPath(splittedParts[1]);
                drawFigure();

                $("#myCanvas").trigger("mousemove");
                console.log(splitPlaneTool);
            }
        }
    }
    else if(splitPlaneTool.isChoosingPart){
        if(ctx.isPointInPath(splitPlaneTool.splittedPart1.path, x, y, "nonzero")) {
            splitPlaneTool.splittedPart1.chosenPart = true;
        }
        if(ctx.isPointInPath(splitPlaneTool.splittedPart2.path, x, y, "nonzero")) {
            splitPlaneTool.splittedPart2.chosenPart = true;
        }
        splitPlaneTool.isChoosingPart = false;
        splitPlaneTool.finishedSplitting = true;
        $('.js-show-bubble').hide();
        $("#recordSplitPlane-btn").hide();

        //TODO: wat met de hoeken
        $("#startPoint").closest(".hidden").show();
        $("#endPoint").closest(".hidden").show();
        $(".js-point1").text(splitPlaneTool.seperationLinePoints.startPoint.edgeLabel);
        $("#startPoint").val(getMovableAxisPoint(splitPlaneTool.seperationLinePoints.startPoint) - padding);
        $(".js-point2").text(splitPlaneTool.seperationLinePoints.endPoint.edgeLabel);
        $("#endPoint").val(getMovableAxisPoint(splitPlaneTool.seperationLinePoints.endPoint) - padding);
        drawFigure();
    }

});

$("#startPoint, #endPoint").on('input', function() {

    var currentPoint = $(this).data("point");
    var oldPointValue = JSON.stringify(splitPlaneTool.seperationLinePoints[currentPoint]);

    //Change seperation line
    var pointValue = splitPlaneTool.seperationLinePoints[currentPoint];
    if(pointValue.edge == "leftEdge" || pointValue.edge =="rightEdge"){
        pointValue.y = Number($(this).val()) + padding;
    }
    else if(pointValue.edge == "topEdge" || pointValue.edge == "bottomEdge"){
        pointValue.x = Number($(this).val()) + padding;
    }

    //Change shapes
    $.each(splitPlaneTool.splittedPart1.points, function(key, value){
        if(JSON.stringify(value) == oldPointValue){
            console.log($(this));
            splitPlaneTool.splittedPart1.points[key] = pointValue;

        }
    })
    $.each(splitPlaneTool.splittedPart2.points, function(key, value){
        if(JSON.stringify(value) == oldPointValue){
            splitPlaneTool.splittedPart2.points[key] = pointValue;

        }
    })
    splitPlaneTool.splittedPart1.path = createPath(splitPlaneTool.splittedPart1.points);
    splitPlaneTool.splittedPart2.path = createPath(splitPlaneTool.splittedPart2.points);
    drawFigure();
});

//=== Variables 3D
const renderScreenWidthLshape = $("#pl24-renderScreen").width();
const renderScreenHeightLshape = $("#pl24-renderScreen").width();
const sceneLp24 = new THREE.Scene();
const cameraLshape = new THREE.PerspectiveCamera( 75, renderScreenWidthLshape / renderScreenHeightLshape, 0.1, 1000 );
cameraLshape.position.z = 5;

const rendererLshape = new THREE.WebGLRenderer();
rendererLshape.setSize( renderScreenWidthLshape, renderScreenHeightLshape );//setSize(width, height)
$("#pl24-renderScreen").append( rendererLshape.domElement );

//Make scrollable
const controlsLshape = new THREE.OrbitControls( cameraLshape, rendererLshape.domElement);

//Light
const ambientLight_Lshape = new THREE.AmbientLight( 0x404040 ); //soft white light
sceneLp24.add( ambientLight_Lshape );

create3D();

//=== 2D
function drawFigure(){
    var widthOfCanvas = $("#myCanvas").width();

    // SET VARIABLES
    //Afmetingen
    heightPlate = Number($("#lengteHoogte").val());
    var lengthA = Number($("#lengtea").val());
    var lengthB = Number($("#lengteb").val());
    var lengthC = Number($("#lengtec").val());
    var lengthAlfa = Number($("#lengteAlfa").val());
    var lengthD = Number($("#lengted").val());
    var nameFaces = [
        "A", "B", "C", "α", "D"
    ];
    lengthFaces = [
      lengthA, lengthB, lengthC, lengthAlfa, lengthD
    ];
    var cornerIndexes = [
        1, 3
    ];
    widthPlate = sumOfArray(lengthFaces);

    // canvas = 1000 en widthPlate = 1000 -> zoom = 1
    // canvas = 1000 en widthPlate = 500 -> zoom = 2
    ctx.clearRect(0, 0, 5000, 5000);
    ctx.restore();
    ctx.save();
    scale = widthOfCanvas/(widthPlate + 2*padding);
    ctx.scale(scale, scale);

    //Lijnfrezingen
    var aantalLijnfrezingen = Number($("#aantalLijnfrezing").val());
    var orientatieLijnfrezingen = $("#orientatieLijnfrezing").val();
    var offsetLijnfrezingen = Number($("#offsetLijnfrezing").val());
    var afstandOnderzijdeLijnfrezingen = Number($("#afstandOnderzijdeLijnfrezing").val());
    //Vormfrezingen
    var aantalVormfrezingen = Number($("#aantalVormfrezing").val());
    var typeVormfrezingen = $("#typeVormfrezing").val();
    var widthSquare = Number($("#widthVormfrezing").val());
    var heightSquare = Number($("#heightVormfrezing").val());
    var offsetVormfrezingen = Number($("#offsetVormfrezing").val());
    var afstandVormfrezing = Number($("#afstandVormLijnfrezing").val());
    var offsetmiddenVormfrezing = Number($("#offsetmiddenVormfrezing").val());
    //Todo: maak variabele sum length faces
    //Sleufen
    var typeSleufen = $("#typeSleufen").val();
    var aantalSleufen = Number($("#aantalSleufen").val());
    var orientatieSleufen = $("#orientatieSleufen").val();
    var offsetSleufen = Number($("#offsetSleufen").val());
    var afstandOnderzijdeSleufen = Number($("#afstandOnderzijdeSleufen").val());
    var afstandZijdeSleufen = Number($("#afstandZijdeSleufen").val());
    var zijdeSleufen = $("#zijdeSleufen").val();
    var breedteGatSleufen = Number($("#breedteGatSleufen").val());

    // DRAWING

    //Draw border (only if not a splitted plane
    ctx.beginPath();
    ctx.strokeStyle = "#000";
    ctx.rect(boundingBoxX, boundingBoxY, widthPlate, heightPlate);
    ctx.stroke();

    //Draw letter names
    ctx.font = "12px Arial";
    drawNames(lengthFaces, nameFaces);

    //Draw folds
    ctx.beginPath();
    ctx.fillStyle = "#f1f1f1";
    drawFolds(lengthFaces, cornerIndexes, heightPlate);

    //Draw lijnfrezing
    ctx.beginPath();
    ctx.strokeStyle = "#D60B52";
    drawLijnFrezingen(aantalLijnfrezingen, orientatieLijnfrezingen, offsetLijnfrezingen, afstandOnderzijdeLijnfrezingen, lengthFaces, heightPlate);

    //Draw vormfrezing
    ctx.beginPath();
    ctx.strokeStyle = "#0bc5d6";
    drawVormFrezingen(aantalVormfrezingen, typeVormfrezingen, widthSquare, heightSquare, offsetVormfrezingen, afstandVormfrezing, offsetmiddenVormfrezing, lengthFaces, heightPlate)

    //Draw sleufen
    ctx.beginPath();
    ctx.strokeStyle = "#40bb23";
    drawSleufen(typeSleufen, aantalSleufen, orientatieSleufen, offsetSleufen, afstandOnderzijdeSleufen, afstandZijdeSleufen, zijdeSleufen, breedteGatSleufen, lengthFaces, heightPlate);

    //Draw sleufgat
    // drawSleufgat();

    //Draw splitPlane
    //Draw splitting line
    if(splitPlaneTool.isSplittingPlane && splitPlaneTool.seperationLinePoints.endPoint.x != null){
        ctx.beginPath();
        ctx.strokeStyle = "#ff0000";
        ctx.moveTo(splitPlaneTool.seperationLinePoints.startPoint.x, splitPlaneTool.seperationLinePoints.startPoint.y);
        ctx.lineTo(splitPlaneTool.seperationLinePoints.endPoint.x, splitPlaneTool.seperationLinePoints.endPoint.y);
        ctx.stroke();
    }
    //Draw splitting planes
    else if(splitPlaneTool.isChoosingPart){
        fillPolygon(splitPlaneTool.splittedPart1.path, "white");
        fillPolygon(splitPlaneTool.splittedPart2.path, "white");
    }
    //Remove the non-chosen part by making it white
    else if(splitPlaneTool.finishedSplitting){
        var chosenPart    = splitPlaneTool.splittedPart1.chosenPart ? splitPlaneTool.splittedPart1 : splitPlaneTool.splittedPart2;
        var remainingPart = splitPlaneTool.splittedPart1.chosenPart ? splitPlaneTool.splittedPart2 : splitPlaneTool.splittedPart1;
        fillPolygon(remainingPart.path, "white");
        strokePolygon(remainingPart.points, "white", 2);
        strokePolygon(chosenPart.points, "black");
    }
}

function drawSleufgat(){

    //Top circle
    var xCenterCircle = 100,
        yCenterCircle = 75,
        radiusCircle = 20
        startingAngleCircle = Math.PI/4 + Math.PI/2,
        endAngleCircle =  Math.PI + Math.PI + Math.PI/4;

    //Line right
    var lengthStraightLine = 50,
        xEndStraightLineRight = xCenterCircle + Math.cos(endAngleCircle) * radiusCircle, //cos is op schaal 1, dus * radiusCircle.
        yEndStraightLineRight = yCenterCircle + radiusCircle/2 + lengthStraightLine;

    //line left
    var xStartStraightLineLeft = xCenterCircle - Math.cos(endAngleCircle) * radiusCircle,
        yStartStraightlineLeft = yCenterCircle + Math.sin(startingAngleCircle) * radiusCircle,
        xEndStraightLineLeft   = xCenterCircle - Math.cos(endAngleCircle) * radiusCircle,
        yEndStraightLineLeft = yCenterCircle + radiusCircle/2 + lengthStraightLine;

    //bottom circle
    var radiusBottomCircle =  (xEndStraightLineRight - xEndStraightLineLeft)/2,
        xCenterBottomCircle = xEndStraightLineLeft + radiusBottomCircle,
        yCenterBottomCircle = yEndStraightLineLeft,
        startingAngleBottomCircle = 0,
        endAngleBottomCircle = Math.PI;


    ctx.beginPath();
    ctx.strokeStyle = "#d67e0b";
    ctx.arc(xCenterCircle, yCenterCircle, radiusCircle, startingAngleCircle, endAngleCircle, false);
    ctx.lineTo(xEndStraightLineRight, yEndStraightLineRight);
    ctx.moveTo(xStartStraightLineLeft, yStartStraightlineLeft);
    ctx.lineTo(xEndStraightLineLeft, yEndStraightLineLeft);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(xCenterBottomCircle, yCenterBottomCircle, radiusBottomCircle, startingAngleBottomCircle, endAngleBottomCircle)
    ctx.stroke();
}

function drawNames(lengthFaces, names){
    posX = boundingBoxX;
    $.each(names, function(index, name){
        posX += lengthFaces[index];
        ctx.fillText(name, posX- lengthFaces[index]/2 - 5, boundingBoxY - 7);
    })
}

function drawFolds(lengthFaces, cornerIndexes, height){
    var xPosition = boundingBoxX;
    $.each(lengthFaces, function(index, face){
        if($.inArray(index, cornerIndexes) !== -1){
            ctx.fillRect(xPosition, boundingBoxY + 1, face, height - 2);
        }
        xPosition += face;
    })
}

function drawLijnFrezingen(aantal, orientatie, offset, afstandOnderzijde, lengthFaces, heightPlate ){
    if(orientatie == "horizontal"){
        for(let i = 0; i < aantal; i++){
            ctx.moveTo(boundingBoxX, boundingBoxY + (heightPlate - afstandOnderzijde - offset * i));
            ctx.lineTo(boundingBoxX + widthPlate, boundingBoxY + (heightPlate - afstandOnderzijde - offset * i));
        }
        ctx.stroke();
    } else{
        for(let i = 0; i < aantal; i++){
            ctx.moveTo(boundingBoxX + afstandOnderzijde + offset * i, boundingBoxY);
            ctx.lineTo(boundingBoxX + afstandOnderzijde + offset * i, boundingBoxY + heightPlate)
        }
        ctx.stroke();
    }
}

function drawVormFrezingen(aantal, type, widthSquare, heightSquare, offset, afstandOnderzijde, offsetMidden, lengthFaces, heightPlate){
    //Vierkanten vertrekken vanuit linker bovenhoek, cirkels vanuit midden

    var helftPlaat = widthPlate/2;
    if(type == "square" ){
        for(let i = 0; i < aantal; i++) {
            ctx.strokeRect(
                boundingBoxX + helftPlaat - widthSquare/2 + offsetMidden,
                boundingBoxY + heightPlate - heightSquare - afstandOnderzijde - ((heightSquare + offset) * i),
                widthSquare,
                heightSquare
        );}
    }
    else if(type == "circle" ){
        for(let i = 0; i < aantal; i++) {
            ctx.beginPath();
            ctx.strokeStyle = "#0bc5d6";
            drawEllipse(
                boundingBoxX + helftPlaat - widthSquare/2 + offsetMidden,
                boundingBoxY + heightPlate - heightSquare - afstandOnderzijde - ((heightSquare + offset) * i),
                widthSquare,
                heightSquare
            )
            ctx.stroke();
        }


    }
}

function drawEllipse(x, y, w, h) {
    var kappa = .5522848,
        ox = (w / 2) * kappa, // control point offset horizontal
        oy = (h / 2) * kappa, // control point offset vertical
        xe = x + w,           // x-end
        ye = y + h,           // y-end
        xm = x + w / 2,       // x-middle
        ym = y + h / 2;       // y-middle

    ctx.beginPath();
    ctx.moveTo(x, ym);
    ctx.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
    ctx.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
    ctx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
    ctx.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
    //ctx.closePath(); // not used correctly, see comments (use to close off open path)
    ctx.stroke();
}

function drawSleufen(typeSleufen, aantal, orientatie, offset, afstandOnderzijde, afstandZijde, zijdeSleufen, breedteGat, lengthFaces, heightPlate ){
    var widthSleuf = typeSleufen == "sleuf" ? 2 : breedteGat;
    var heightSleuf = typeSleufen == "sleuf" ? 4 : breedteGat;
    var afstandZijdeHerberekend = zijdeSleufen == "left" ? afstandZijde : widthPlate - widthSleuf - afstandZijde - widthSleuf;
    for(let i = 0; i < aantal; i++){

        if(typeSleufen == "sleuf"){
            ctx.beginPath();
            ctx.strokeStyle = "#40bb23";
            ctx.arc(boundingBoxX + widthSleuf/2 + afstandZijdeHerberekend, boundingBoxY + (heightPlate - afstandOnderzijde  - (heightSleuf + widthSleuf)- (offset + (heightSleuf + 2*widthSleuf)) * i), widthSleuf, degToRad(0), degToRad(180), true);
            ctx.lineTo(boundingBoxX -widthSleuf + widthSleuf/2 + afstandZijdeHerberekend, boundingBoxY + (heightPlate - afstandOnderzijde  - (heightSleuf + widthSleuf)- (offset + (heightSleuf + 2*widthSleuf)) * i) +heightSleuf)
            ctx.moveTo(boundingBoxX + widthSleuf + widthSleuf/2 + afstandZijdeHerberekend, boundingBoxY + (heightPlate - afstandOnderzijde  - (heightSleuf + widthSleuf)- (offset + (heightSleuf + 2*widthSleuf)) * i) + heightSleuf);
            ctx.arc(boundingBoxX + widthSleuf/2 + afstandZijdeHerberekend, boundingBoxY + (heightPlate - afstandOnderzijde  - (heightSleuf + widthSleuf)- (offset + (heightSleuf + 2*widthSleuf)) * i) + heightSleuf, widthSleuf, degToRad(0), degToRad(180), false);
            ctx.moveTo(boundingBoxX + widthSleuf + widthSleuf/2 + afstandZijdeHerberekend, boundingBoxY + (heightPlate - afstandOnderzijde  - (heightSleuf + widthSleuf)- (offset + (heightSleuf + 2*widthSleuf)) * i) + heightSleuf);
            ctx.lineTo(boundingBoxX + widthSleuf + widthSleuf/2 + afstandZijdeHerberekend, boundingBoxY + (heightPlate - afstandOnderzijde  - (heightSleuf + widthSleuf)- (offset + (heightSleuf + 2*widthSleuf)) * i));
            ctx.stroke();
        }
        else if(typeSleufen == "verzonkenGat"){
            ctx.beginPath();
            ctx.strokeStyle = "#40bb23";
            ctx.arc(boundingBoxX + afstandZijdeHerberekend + widthSleuf, boundingBoxY + (heightPlate - afstandOnderzijde - heightSleuf - (offset + (heightSleuf +heightSleuf)) * i), widthSleuf, 2 * Math.PI, false);
            ctx.stroke();
            ctx.beginPath();
            ctx.strokeStyle = "#40bb23";
            ctx.arc(boundingBoxX + afstandZijdeHerberekend + widthSleuf, boundingBoxY + (heightPlate - afstandOnderzijde - heightSleuf - (offset + (heightSleuf +heightSleuf)) * i), widthSleuf/2, 2 * Math.PI, false);
            ctx.stroke();
        }
        else if(typeSleufen == "gewoonGat"){
            ctx.beginPath();
            ctx.strokeStyle = "#40bb23";
            ctx.arc(boundingBoxX + afstandZijdeHerberekend + widthSleuf, boundingBoxY + (heightPlate - afstandOnderzijde - heightSleuf - (offset + (heightSleuf +heightSleuf)) * i), widthSleuf, 2 * Math.PI, false);
            ctx.stroke();
        }
    }
}

//returns true if you drew within the figure
function drawWithinFigure(x, y){

    if( x > boundingBoxY - allowedMargin &&
        x < boundingBoxY + heightPlate + allowedMargin &&
        y > boundingBoxX - allowedMargin &&
        y < boundingBoxX + widthPlate + allowedMargin
    ){
        return true;
    }
    return false;
}

function drawClicks(x, y){
    if(recordedClicks.length > 0){
        ctx.strokeStyle = "#300bd6";
        var last = recordedClicks[recordedClicks.length - 1];
        ctx.beginPath();
        ctx.moveTo(last.x, last.y);
        ctx.lineTo(x, y);
        ctx.stroke();
    }
    recordedClicks.push({ x: x, y: y });
}

//=== 3D
function create3D(){
    const geometry = new THREE.BufferGeometry();
    geometry.vertices.push(
        new THREE.Vector3(-1, -1,  1),  // 0
        new THREE.Vector3( 1, -1,  1),  // 1
        new THREE.Vector3(-1,  1,  1),  // 2
        new THREE.Vector3( 1,  1,  1),  // 3
        new THREE.Vector3(-1, -1, -1),  // 4
        new THREE.Vector3( 1, -1, -1),  // 5
        new THREE.Vector3(-1,  1, -1),  // 6
        new THREE.Vector3( 1,  1, -1),  // 7
    );
    geometry.faces.push(
        // front
        new THREE.Face3(0, 3, 2),
        new THREE.Face3(0, 1, 3),
        // right
        new THREE.Face3(1, 7, 3),
        new THREE.Face3(1, 5, 7),
        // back
        new THREE.Face3(5, 6, 7),
        new THREE.Face3(5, 4, 6),
        // left
        new THREE.Face3(4, 2, 6),
        new THREE.Face3(4, 0, 2),
        // top
        new THREE.Face3(2, 7, 6),
        new THREE.Face3(2, 3, 7),
        // bottom
        new THREE.Face3(4, 1, 0),
        new THREE.Face3(4, 5, 1),
    );
    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);
}

function findClosestEdgePoint(x, y){
    //         .      .
    //         .      .
    //         .      .
    //         |      |
    //      1  |   2  |  3
    // ...-----+------+-----...
    //         |      |
    //      4  |   5  |  6
    //         |      |
    // ...-----+------+-----...
    //      7  |   8  |  9
    //         |      |
    //         .      .
    //         .      .
    //         .      .

    //in part 1
    if(x <= boundingBoxX && y <= boundingBoxY){
        return { x: boundingBoxX, y: boundingBoxY, edge: 'topLeftCorner', edgeLabel: 'linker bovenhoek' };
    }
    //in part 2
    if(x >= boundingBoxX && x <= boundingBoxX + widthPlate &&
       y <= boundingBoxY) {
        return { x: x, y: boundingBoxY, edge: 'topEdge', edgeLabel: 'bovenzijde' };
    }
    //in part 3
    if(x >= boundingBoxX + widthPlate &&
        y <= boundingBoxY) {
        return { x: boundingBoxX + widthPlate, y: boundingBoxY, edge: 'topRightCorner', edgeLabel: 'rechter bovenhoek' };
    }
    //in part 4
    if(x <= boundingBoxX &&
        y >= boundingBoxY && y <= boundingBoxY + heightPlate){
        return { x: boundingBoxX, y: y, edge: 'leftEdge', edgeLabel: 'linkerzijde' };
    }
    //in part 5
    if( x > boundingBoxX  &&  x < boundingBoxX + widthPlate &&
        y > boundingBoxY && y < boundingBoxY + heightPlate
    ){
        var topOrBottom = y < boundingBoxY + (heightPlate/2) ? 'top' : 'bottom';
        var leftOrRight= x < boundingBoxX + widthPlate/2 ? 'left' : 'right';

        var distanceTopOrBottom = topOrBottom == 'top' ? y - boundingBoxY : boundingBoxY + heightPlate - y;
        var distanceLeftOrRight = leftOrRight == 'left' ? x - boundingBoxX : boundingBoxX + widthPlate - x;

        if(topOrBottom == 'top' && distanceTopOrBottom < distanceLeftOrRight){
            return { x: x, y: boundingBoxY, edge: 'topEdge', edgeLabel: 'bovenzijde' };
        }
        if(topOrBottom == 'bottom' && distanceTopOrBottom < distanceLeftOrRight){
            return { x: x, y: boundingBoxY + heightPlate, edge: 'bottomEdge', edgeLabel: 'onderzijde' };
        }
        if(leftOrRight == 'left' && distanceLeftOrRight < distanceTopOrBottom){
            return { x: boundingBoxX, y: y, edge: 'leftEdge', edgeLabel: 'linkerzijde' };
        }
        if(leftOrRight == 'right' && distanceLeftOrRight < distanceTopOrBottom){
            return { x: boundingBoxX + widthPlate, y: y, edge: 'rightEdge', edgeLabel: 'rechterzijde' };
        }
    }
    //in part 6
    if(x >= boundingBoxX + widthPlate &&
        y >= boundingBoxY && y <= boundingBoxY + heightPlate){
        return { x: boundingBoxX + widthPlate, y: y, edge: 'rightEdge', edgeLabel: 'rechterzijde' };
    }
    //in part 7
    if(x <= boundingBoxX &&
        y >= boundingBoxY + heightPlate){
        return { x: boundingBoxX, y: boundingBoxY + heightPlate, edge: 'bottomLeftCorner', edgeLabel: 'linker onderzijde' };
    }
    //in part 8
    if(x >= boundingBoxX && x <= boundingBoxX + widthPlate &&
        y >= boundingBoxY + heightPlate){
        return { x: x, y: boundingBoxY + heightPlate, edge: 'bottomEdge', edgeLabel: 'onderzijde' };
    }
    //in part 9
    if(x >= boundingBoxX + widthPlate &&
        y >= boundingBoxY + heightPlate){
        return { x: boundingBoxX + widthPlate, y: boundingBoxY + heightPlate, edge: 'bottomRightCorner', edgeLabel: 'rechter onderzijde' };
    }
}

function createTwoParts(){
    var sidesOrCorners = [splitPlaneTool.seperationLinePoints.startPoint.edge, splitPlaneTool.seperationLinePoints.endPoint.edge];
    var pointsShape1 = [];
    var pointsShape2 = [];

    var topLeftCorner     = {x: boundingBoxX,                           y: boundingBoxY,          edge: "topLeftCorner"    , edgeLabel: "linker bovenhoek"  },
        topRightCorner    = {x: boundingBoxX + widthPlate, y: boundingBoxY,          edge: "topRightCorner"   , edgeLabel: "rechter bovenhoek" },
        bottomRightCorner = {x: boundingBoxX + widthPlate, y: boundingBoxY + heightPlate, edge: "bottomRightCorner", edgeLabel: "rechter onderhoek" },
        bottomLeftCorner  = {x: boundingBoxX,                           y: boundingBoxY + heightPlate, edge: "bottomLeftCorner" , edgeLabel: "linker onderhoek"  };

    //Line from top to bottom edge
    if(sidesOrCorners.includes("topEdge") && sidesOrCorners.includes("bottomEdge")){
        var chosenTopValue    = splitPlaneTool.seperationLinePoints.startPoint.edge == "topEdge"    ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;
        var chosenBottomValue = splitPlaneTool.seperationLinePoints.startPoint.edge == "bottomEdge" ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;
        pointsShape1 = [
            topLeftCorner,
            bottomLeftCorner,
            {x: chosenBottomValue.x, y: chosenBottomValue.y, edge: "bottomEdge", edgeLabel: "onderzijde"},
            {x: chosenTopValue.x,    y: chosenTopValue.y,    edge: "topEdge", edgeLabel: "bovenzijde"},
        ];
        pointsShape2 = [
            {x: chosenBottomValue.x, y: chosenBottomValue.y, edge: "bottomEdge", edgeLabel: "onderzijde"},
            {x: chosenTopValue.x,    y: chosenTopValue.y,    edge: "topEdge", edgeLabel: "bovenzijde"},
            topRightCorner,
            bottomRightCorner,
        ];
        return [pointsShape1, pointsShape2];
    }

    //Line from left to right edge
    if(sidesOrCorners.includes("leftEdge") && sidesOrCorners.includes("rightEdge")){
        var chosenLeftValue  = splitPlaneTool.seperationLinePoints.startPoint.edge == "leftEdge"  ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;
        var chosenRightValue = splitPlaneTool.seperationLinePoints.startPoint.edge == "rightEdge" ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;
        pointsShape1 = [
            topLeftCorner,
            topRightCorner,
            {x: chosenRightValue.x, y: chosenRightValue.y, edge: "rightEdge", edgeLabel: "rechterzijde"},
            {x: chosenLeftValue.x,  y: chosenLeftValue.y,  edge: "leftEdge",  edgeLabel: "linkerzijde" },
        ];
        pointsShape2 = [
            {x: chosenRightValue.x, y: chosenRightValue.y, edge: "rightEdge", edgeLabel: "rechterzijde"},
            {x: chosenLeftValue.x,  y: chosenLeftValue.y,  edge: "leftEdge",  edgeLabel: "linkerzijde"},
            bottomLeftCorner,
            bottomRightCorner,
        ];

        return [pointsShape1, pointsShape2];
    }

    //Line from topLeftCorner to bottomRightCorner
    if(sidesOrCorners.includes("topLeftCorner") && sidesOrCorners.includes("bottomRightCorner")){
        pointsShape1 = [
            topLeftCorner,
            topRightCorner,
            bottomRightCorner,
        ];
        pointsShape2 = [
            topLeftCorner,
            bottomLeftCorner,
            bottomRightCorner,
        ];
        return [pointsShape1, pointsShape2];
    }

    //Line from topRightCorner to bottomLeftCorner
    if(sidesOrCorners.includes("topRightCorner") && sidesOrCorners.includes("bottomLeftCorner")){
        pointsShape1 = [
            topLeftCorner,
            topRightCorner,
            bottomLeftCorner,
        ];
        pointsShape2 = [
            topRightCorner,
            bottomRightCorner,
            bottomLeftCorner,
        ];
        return [pointsShape1, pointsShape2];
    }

    //Line from leftEdge to topEdge
    if(sidesOrCorners.includes("leftEdge") && sidesOrCorners.includes("topEdge")){
        var chosenTopValue  = splitPlaneTool.seperationLinePoints.startPoint.edge == "topEdge"  ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;
        var chosenLeftValue = splitPlaneTool.seperationLinePoints.startPoint.edge == "leftEdge" ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;

        pointsShape1 = [
            topLeftCorner,
            {x: chosenTopValue.x,  y: chosenTopValue.y,   edge: "topEdge",  edgeLabel: "bovenzijde" },
            {x: chosenLeftValue.x,  y: chosenLeftValue.y, edge: "leftEdge", edgeLabel: "linkerzijde"},
        ];
        pointsShape2 = [
            {x: chosenTopValue.x,   y: chosenTopValue.y,  edge: "topEdge", edgeLabel: "bovenzijde"},
            topRightCorner,
            bottomRightCorner,
            bottomLeftCorner,
            {x: chosenLeftValue.x,  y: chosenLeftValue.y, edge: "leftEdge", edgeLabel: "linkerzijde"},
        ];
        return [pointsShape1, pointsShape2];
    }

    //Line from topEdge to rightEdge
    if(sidesOrCorners.includes("topEdge") && sidesOrCorners.includes("rightEdge")){
        var chosenTopValue   = splitPlaneTool.seperationLinePoints.startPoint.edge == "topEdge"   ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;
        var chosenRightValue = splitPlaneTool.seperationLinePoints.startPoint.edge == "rightEdge" ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;

        pointsShape1 = [
            {x: chosenTopValue.x,   y: chosenTopValue.y,   edge: "topEdge", edgeLabel: "bovenzijde"},
            topRightCorner,
            {x: chosenRightValue.x, y: chosenRightValue.y, edge: "rightEdge", edgeLabel: "rechterzijde"},
        ];
        pointsShape2 = [
            {x: chosenRightValue.x, y: chosenRightValue.y, edge: "rightEdge", edgeLabel: "rechterzijde"},
            bottomRightCorner,
            bottomLeftCorner,
            topLeftCorner,
            {x: chosenTopValue.x,   y: chosenTopValue.y,   edge: "topEdge", edgeLabel: "bovenzijde"},
        ];
        return [pointsShape1, pointsShape2];
    }

    //Line from leftEdge to bottomEdge
    if(sidesOrCorners.includes("leftEdge") && sidesOrCorners.includes("bottomEdge")){
        var chosenLeftValue   = splitPlaneTool.seperationLinePoints.startPoint.edge == "leftEdge"   ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;
        var chosenBottomValue = splitPlaneTool.seperationLinePoints.startPoint.edge == "bottomEdge" ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;

        pointsShape1 = [
            {x: chosenLeftValue.x,   y: chosenLeftValue.y,  edge: "leftEdge", edgeLabel: "linkerzijde"},
            bottomLeftCorner,
            {x: chosenBottomValue.x, y: chosenBottomValue.y, edge: "bottomEdge", edgeLabel: "onderzijde"},
        ];
        pointsShape2 = [
            {x: chosenLeftValue.x,   y: chosenLeftValue.y,   edge: "leftEdge", edgeLabel: "linkerzijde"},
            topLeftCorner,
            topRightCorner,
            bottomRightCorner,
            {x: chosenBottomValue.x, y: chosenBottomValue.y, edge: "bottomEdge", edgeLabel: "onderzijde"},
        ];
        return [pointsShape1, pointsShape2];
    }

    //Line from bottomEdge to rightEdge
    if(sidesOrCorners.includes("bottomEdge") && sidesOrCorners.includes("rightEdge")){
        var chosenBottomValue = splitPlaneTool.seperationLinePoints.startPoint.edge == "bottomEdge" ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;
        var chosenRightValue  = splitPlaneTool.seperationLinePoints.startPoint.edge == "rightEdge"  ? splitPlaneTool.seperationLinePoints.startPoint : splitPlaneTool.seperationLinePoints.endPoint;

        pointsShape1 = [
            {x: chosenBottomValue.x, y: chosenBottomValue.y, edge: "bottomEdge", edgeLabel: "onderzijde"},
            bottomRightCorner,
            {x: chosenRightValue.x,  y: chosenRightValue.y,  edge: "rightEdge", edgeLabel: "rechterzijde"},
        ];
        pointsShape2 = [
            {x: chosenBottomValue.x, y: chosenBottomValue.y, edge: "bottomEdge", edgeLabel: "onderzijde"},
            bottomLeftCorner,
            topLeftCorner,
            topRightCorner,
            {x: chosenRightValue.x,  y: chosenRightValue.y,  edge: "rightEdge", edgeLabel: "rechterzijde"},
        ];
        return [pointsShape1, pointsShape2];
    }
}

//=== General functions
function sumOfArray(arr){
    var sum = 0;
    $.each(arr,function(){sum+=parseFloat(this) || 0;});
    return sum;
}

function degToRad(deg){
    var pi = Math.PI;
    return deg * (pi/180);
}

function createPath(points){
    if (points.length > 0) {
        var point = points[0];

        var path = new Path2D();
        path.moveTo(point.x, point.y);   // point 1
        for (var i = 1; i < points.length; ++i) {
            point = points[i];
            path.lineTo(point.x, point.y);
        }

        return path;
    }
}

function fillPolygon(path, color) {
    ctx.fillStyle = color; // all css colors are accepted by this property
    ctx.fill(path);
}

function strokePolygon(points, color, strokeWidth = 1){
    if (points.length > 0) {
        ctx.strokeStyle = color; // all css colors are accepted by this property

        var point = points[0];

        ctx.beginPath();
        ctx.moveTo(point.x, point.y);   // point 1

        for (var i = 1; i < points.length; ++i) {
            point = points[i];

            ctx.lineTo(point.x, point.y);
        }
        ctx.closePath();
        ctx.lineWidth = strokeWidth;
        ctx.stroke();
    }
}

function hatchPolygon(points){
    if (points.length > 0) {
        var point = points[0];

        ctx.beginPath();
        ctx.moveTo(point.x, point.y);   // point 1

        for (var i = 1; i < points.length; ++i) {
            point = points[i];

            ctx.lineTo(point.x, point.y);
        }
        ctx.clip();

        var hatchImage = new Image();
        hatchImage.onload = function() {
            ctx.drawImage( hatchImage, 0, 0, $("#myCanvas").width() / scale, $("#myCanvas").height() / scale);
        }
        hatchImage.src = "../../images/hatchPattern01.jpg";
    }
}

//The point on an axis can move on only one axis without leaving the edge, find that axis point
function getMovableAxisPoint(point){
    if(point.edge == "leftEdge" || point.edge =="rightEdge"){
        return point.y;
    }
    else if(point.edge == "topEdge" || point.edge == "bottomEdge"){
        return point.x;
    }
}

# Threejs for ursus #

### Importeren ###

Geïmporteerde objecten zijn enkel buffergeometries ipv gewone geometries. 
Buffer is geoptimaliseerd voor klein te zijn, gewoon meer om aan te passen.

> IDEA: als een gewone geometry gemakkelijker is om mee te werken, 
> kunnen we dan niet alle punten kopieren van de buffer en in een gewone steken. 

imported geometry van blender werkt precies toch in quads.

### SetIndex ###

![img.png](README-images/createBuffer.png)

Verander bovenstaande vertices via setIndex naar een meer overzichtelijke matrix.

    const vertices = new Float32Array([
       -1, -1,  1,
        1, -1,  1,
        1,  1,  1,

       // 1, 1, 1 -> deze lijn is weg want we referencen deze 
       -1,  1,  1
       // 1, 1, 1 -> deze lijn is weg want we referencen deze
    ]);

    geometry.setIndex([
        0, 1, 2,  2, 3, 0
    ]);

is gelijk aan

    const vertices = new Float32Array([
       -1, -1,  1,
        1, -1,  1,
        1,  1,  1,
       -1,  1,  1

    ]);

    geometry.setIndex([
        0, 1, 2,  2, 3, 0
    ]);

![](../../../Downloads/IMG_4783.png)

### titel

We kunnen met de exact juiste afmetingen werken.

> Kunnen we via dat niet programmatically ophalen welke locaties (xyz) welke vertices zijn op het object.

![img.png](README-images/xyzGelijkBlender.png)
![img.png](README-images/xyzGelijkBrowser.png)

### Stappen die ik doe (en zou moeten automatiseren)

- Identificeer welke locaties (x,y,z) welke vertices zijn.
- Indexeren (nog niet gedaan bij lShape, loaded gltf) (vind dubbele en verwijder, bij nieuwe array tel je telkens op tenzij het al voorkomt)
- Link elk puntje aan een input -> inputs evt omzetten naar lengtes ipv xyz posities
  - Welke edge (2 vertices) moeten op welke as verschuiven om lengte x langer te maken?
    - Beide vertices gaan op de veranderbare as eenzelfde waarde hebben 
- Unwrapping in blender, 
- Alpha map maken voor gaten

<?php

/**
 * Authentication actions.
 *
 * @package    globe
 * @subpackage authentication
 * @author     Ellen
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class authenticationActions extends sfActions
{
    public $reqData = null;
    
    public function preExecute()
    {
        $this->getResponse()->setHttpHeader('content-type', 'application/json');
        $this->reqData =  json_decode($this->getRequest()->getContent(), true);
    }
    
    public function executeRegister(sfWebRequest $request)
    {
        $_email                    = $this->reqData["mail"];
        $_gdpr_permission          = $this->reqData["gdprPermission"];
        $_cross_selling_permission = $this->reqData["crossSellingPermission"];
        $_phone_id                 = $this->reqData["device_id"];
        $_os                       = $this->reqData["phone_os"];
        $letMeTrough = false;
        
        if(trim(strtolower($_email)) == "appledemoaccount@aware.be"){
            
            $_email = "lukvanlokeren@telenet.be";
            $letMeTrough = true;
        }
        
        if(trim($_email) === "" ) {
            set_error_handler(ErrorHandling::noRequestData(__LINE__, __FILE__, 'No email found. ReqData' . json_encode($this->reqData)));
        }

        $user = Doctrine::getTable("sfGuarduser")->findOneBy("email_address", Crypt::encrypt(strtolower($_email)));
        
        if(!$user ){
            set_error_handler(ErrorHandling::noRequestData(__LINE__, __FILE__, 'Geen gebruiker gevonden met dit mailadres. ReqData: ' .json_encode($this->reqData)));
        }
        
        if(!$_gdpr_permission) {
            set_error_handler(ErrorHandling::noRequestData(__LINE__, __FILE__, 'GDPR not checked. ReqData: ' . json_encode($this->reqData)));
        }
        
        $user->setGdprPermission(date("Y-m-d H:i:s"));
        $user->setCrossSellingPermission($_cross_selling_permission ? date("Y-m-d H:i:s") : null);
    
        $user->save();

        $userDevices = Doctrine::getTable("userDevice")->findBy("user_id", $user->getId());
        
        //Check if device already exists
        $found = false;
        $foundDevice = null;
        foreach( $userDevices as $userDevice ){
            if( $userDevice->getPhoneId() === $_phone_id ){
                $found = true;
                $foundDevice = $userDevice;
            }
        }

        //Create device
        

        if(!$found) {
            $device = new UserDevice();
            
            $device->setUserId($user->getId());
            $device->setPhoneId($_phone_id);
            $device->setPhoneOs($_os);
            $device->setHash(hash('md5', microtime() . rand(10, 99) . $_phone_id . dechex(time()))); }
        else {
            $device = $foundDevice; 
        }

        $device->setEnabled(false);

        $device->save();
        
        $urlProtocol = explode("/", $_SERVER["SERVER_PROTOCOL"], 2)[0];
        $urlName     = $_SERVER['SERVER_NAME'];
        $url         = $urlProtocol . '://' . $urlName;
        
        
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "Bcc: app.aep@aware.be\r\n";
        
        //APP STORE TESTING TEMPORARILY GETS THROUGH WITHOUT EMAIL
        if($letMeTrough 
        //   strtolower($_email) == "pascal@aware.be" 
        //    strtolower($_email) == "lukvanlokeren@telenet.be" ||
        //    strtolower($_email) == "febe.stradiot@gmail.com" ||
        // || strtolower($_email) == "sabine@topvakantie.be" 
        //||    strtolower($_email) == "sarah.deruyter@topvakantie.be" 
        //    strtolower($_email) == "amelle@fake.be" ||
        // ||    strtolower($_email) == "cedric@topvakantie.be"
        //    strtolower($_email) == "maya.degreef.1996@gmail.com" ||
        //    strtolower($_email) == "pinocooreman@hotmail.com" ||
        //    strtolower($_email) == "lukasdewilde0@gmail.com"
        // || strtolower($_email) == "sandracammaerts@hotmail.com"
        // ||
        //strtolower($_email) == "__fakeb793dc3e30cbaa9c@aware.be"
        // || strtolower($_email) == "kevinrooms@live.be"
        // || strtolower($_email) == "emmaus@topvakantie.be"
        //|| strtolower($_email) == "fie.vanderauwera@hotmail.com"
        //|| strtolower($_email) == "nathalie.vanoosterwijck@telenet.be"
        // || strtolower($_email) == "erik.schollaert@skynet.be"
        // || strtolower($_email) == "astrid.deweirdt@gmail.com"
        // || strtolower($_email) == "__fakee471507d1dc3e4cf@aware.be"
        //|| strtolower($_email) == "isabel.vanstijvoort@vlaanderen.be"
        // || strtolower($email) == "robbie.cordonnier@telenet.be"
           || strtolower($email) == "katrien.dujardin@telenet.be"
          ){
            //executeAccept function
            
            $device->setEnabled(true);
            $device->save();
            
            $response = $this->renderText(json_encode(array(
                'Success' => true
            ))); 
            return $response; 
        }
        
        
        if(isset($this->reqData["locale"])){
            $_locale = $this->reqData["locale"];    
        }
        else{
            $_locale = 'nl';    
        }
        
        if($_locale == 'nl'){
            $msg = "
                <html>
                    <head>
                        <title>Bevestiging Topvakantie app</title>
                    </head>
                    <body>
                        <p>Beste,</p>
                        <p>Welkom bij de Top Vakantie app. Klik op onderstaande link om uw e-mailadres te bevestigen en zo toegang te krijgen tot de app.</p>
                        <a href='" . $url . "/client.php/app/accept/login/". $_locale . "/" .  $device->getHash() ."' >Bevestig e-mailadres</a>
                        <p>Met vriendelijke groeten</p>
                        <p>Top Vakantie vzw</p>
                        <img src='https://www.topvakantie.be/images/2015/xlogo.png.pagespeed.ic.JIqM8KtZje.png' />
                    </body>
                </html>
                ";
           
            mail($_email, "Bevestig mailadres", $msg, $headers, "-f no-reply@topvakantie.be");
            //mail("ellen.vervaet@aware.be", "Bevestig mailadres", $msg, $headers, "-f no-reply@topvakantie.be");
        }
        else {
             $msg = "
                <html>
                    <head>
                        <title>Confirmation application Vacances Vivantes</title>
                    </head>
                    <body>
                        <p>Bonjour,</p>
                        <p>Bienvenue sur l'application de Vacances Vivantes. Cliquez sur le lien ci-dessous pour confirmer votre adresse e-mail:</p>
                        <a href='" . $url . "/client.php/app/accept/login/". $_locale . "/" . $device->getHash() ."' >Confirmez adresse e-mail</a>
                    </body>
                </html>
                ";
           
            mail($_email, "Confirmez adresse e-mail", $msg, $headers, "-f no-reply@vacancesvivantes.be");
            //mail("ellen.vervaet@aware.be", "Confirmez adresse e-mail", $msg, $headers, "-f no-reply@vacancesvivantes.be");
        }
        
         $response = $this->renderText(json_encode(array(
            'Success' => true
        ))); 
        return $response; 
        
    }

    // public function executeAccept(sfWebRequest $request)
    // {

    //     $device = Doctrine::getTable("userDevice")->findOneBy("hash", $request->getParameter("hash"));
        
    //     if( $request->getParameter("locale") == 'fr'){
    //         $_locale = $request->getParameter("locale");    
    //     }
    //     else{
    //         $_locale = 'nl';    
    //     }

    //     $device->setEnabled(true);
    //     $device->save();

    //     $response = $this->getResponse();
    //     $response->setHttpHeader("Content-Type", 'text/html');
        
    //     if($_locale == 'nl'){
    //         return 'MailConfirmed';
    //     }
    //     else{
    //         return 'MailConfirmedFr';
    //     }
    // }

    public function executeRegisterCheck(sfWebRequest $request)
    {
        $device_id = $this->reqData;

        $deviceIsRegistered = Doctrine::getTable("userDevice")->checkRegistration($device_id);

        if( $deviceIsRegistered ) {
            
            $user_id = Doctrine::getTable('userDevice')->findOneBy('phone_id', $device_id)->getUserId();
            $user = Doctrine::getTable('sfGuarduser')->findOneById($user_id);
            
            $freshToken = hash('sha256', base64_encode(openssl_random_pseudo_bytes(20)));
            $user->setAPIToken($freshToken);
            $user->save();
            
            $response = $this->renderText(json_encode(array(
                'Success' => true,
                'Token'   => $freshToken,
                'Name'    => $user->getProfile()->getFirstName()//Get in seperate request because has nothing to do with register but is needed to show name when displaying activities
                )));
            return $response; 
        }
        else {
            return  $this->renderText(json_encode(array(
                'Success' => false
            ))); 
        }
    }
}
